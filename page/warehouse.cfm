<cfoutput>
    <script type="text/java">
        function myFunction() {
            location.reload();
        }
    </script>

		<cfquery name="items" datasource="inventnet">
			SELECT * FROM `items`;
		</cfquery>
    <cflock scope="Session" type="exclusive" timeout="5"throwontimeout="true">
      <cfset #session.me# = #session.me#>
    </cflock>
    <cflock scope="Session" type="exclusive" timeout="5"throwontimeout="true">
      <cfset #session.roll# = #session.roll#>
    </cflock>


		
<!----------------------------------------------------------------------------------->

<!---------------------------------user interface-------------------------------------------->
    <div style="margin-top:0px;background-color:##fff; position:fixed;z-index:1000; width:100%; box-shadow: 2px 3px 3px   ##ccc;">
      <!-----add items-------------->
    <cfif isdefined("form.submit")>
      <cfquery name="items" datasource="inventnet">
        SELECT * FROM `items` WHERE `itemName`= "#form.itemname#";
      </cfquery>
      <cfif items.recordcount greater than  0>

      <cfquery name="items" datasource="inventnet">
      SELECT * FROM `items`;
      </cfquery>
            <cfset #form.itemname#=""/>
            <cfset #form.description#=""/>
            <cfset #form.Quantity#=""/>
            <cfset #form.price#=""/>
            <cfset #form.category#=""/>
            <cfset #form.itemname#=""/>
          
        <cfelse>
          <cfset totalcost= #form.costprice# * #form.Quantity#>
          <cfset totalsale= #form.saleprice# * #form.Quantity#>
          <cfset profit= #totalsale# - #totalcost#>
          <cfquery name="additem" datasource="inventnet">
            INSERT INTO `items` (`ItemName`, 
                                `Description`, 
                                `QTYavailable`, 
                                `CostPrice`,
                                `SalesPrice`,
                                `TotalCostPrice`,
                                `TotalSalesPrice`,
                                `Profit`,
                                `DateAdded`,
                                `TimeAdded`)
                  VALUES (
                    <cfqueryparam cfsqltype="CF_SQL_varchar" value='#form.itemname#'>, 
                    <cfqueryparam cfsqltype="CF_SQL_varchar" value='#form.description#'>,
                    <cfqueryparam cfsqltype="CF_SQL_INTEGER" value='#form.Quantity#'>,
                    <cfqueryparam cfsqltype="CF_SQL_INTEGER" value='#form.costprice#'>,
                    <cfqueryparam cfsqltype="CF_SQL_INTEGER" value='#form.saleprice#'>,
                    <cfqueryparam cfsqltype="CF_SQL_varchar" value='#totalcost#'>,
                    <cfqueryparam cfsqltype="CF_SQL_varchar" value='#totalsale#'>,
                    <cfqueryparam cfsqltype="CF_SQL_varchar" value='#profit#'>,
                    <cfqueryparam cfsqltype="CF_SQL_varchar" value='#dateformat(now(), "yyyy-mm-dd")#'>,
                    <cfqueryparam cfsqltype="CF_SQL_varchar" value= '#TimeFormat(now(), "HH:MM:SS")#'>
                    )
          </cfquery><br/>
            <div style="margin-left:500px; width:450px; border-radius:10px; padding-left:80px; background-color:##4DA64D;color:##003300;"><b>You have successfully added #form.itemname# to stock</b></div>
          <cfquery name="items" datasource="inventnet">
              SELECT * FROM `items`;
          </cfquery>
          <script type="text/java">
          $('##myModal').on('hide.bs.modal', function () {
              $('##myModal').removeData();
            })
          </script>
      </cfif>
    </cfif>
<!-------------------------------------end of Edit items----------------------------->
<cfif isdefined("form.edit")>
  <cfset edittotalcost = #form.costprice#*#form.Quantity#>
   <cfset edittotalsales = #form.saleprice#*#form.Quantity#>
    <cfset editprofit = #edittotalsales#-#edittotalcost#>
  <cfquery name="Edit" datasource="inventnet">
   UPDATE `items`
            SET ItemName = '#form.itemname#', Description="#form.description#", QTYavailable="#form.Quantity#",
                CostPrice = "#form.costprice#", SalesPrice="#form.saleprice#", TotalCostPrice="#edittotalcost#",
                TotalSalesPrice = "#edittotalsales#", Profit="#editprofit#"
            WHERE  `itemName`="#form.itemname#"
  </cfquery>
  <br/> <div style="margin-left:490px; width:500px; border-radius:10px; padding-left:150px; background-color:##4DA64D;color:##003300;"><b>Item was  successfully Edited</b></div>
   <cfquery name="items" datasource="inventnet">
      SELECT * FROM `items`;
  </cfquery>
 
</cfif>
    		<h3 align="center" style="color:##800000;padding:20px;"><b>ITEMS LIST</b></h3>
    		<div class="col-md-3">
    			<form>
    				<div class="form-group">
    					<input class="form-control" type="text" name="search" value="" placeholder="Search..."></input>
    				</div>
    			</form>
    		</div>
    		<div class="col-md-4"></div>
    		<div class="col-md-5" style="padding-left:90px;">
    			
             <a href="?pg=warehouse"><button type="button" class="btn btn-default"><i class="fa fa-refresh"></i> Refresh</button></a>
            
            <button type="button" data-toggle="modal" data-target="##additem" data-refresh="true" class="btn btn-danger"><i class="fa fa-plus"></i> Add item</button> 
    				<button type="button" data-toggle="modal" data-target="##search" data-refresh="true" class="btn btn-primary"><i class="fa fa-search"></i> Search Item</button> 
            <button type="button" class="btn btn-md btn-danger" data-toggle="modal" data-target="##Edit" data-refresh="true" ><i class="fa fa-edit"></i>&ensp;Edit Item</button>

          </div><br/><br/>
    		<br/>    
      </div>
  <div style="margin-top:150px;">
    <cfloop query="items">
      <cfif #QTYavailable# == 0>
        <br/><div style="margin-left:50px; width:450px; border-radius:10px; padding-left:85px; background-color:##FF0000;color:##ffffff;"><b>!! YOU ARE OUT OF STOCK ON ITEM: #ItemName# !!</b></div>
        <cfelse>
      </cfif>
    </cfloop>
        <div class="col-md-12"style="padding:40px; padding-buttom:5px;" id="resultTable">
          <table class="table table-striped" cellspacing="0" cellpadding="0">
            <thead>
                <tr>
                  <th>##Item No.</td>
                  <th><i class="fa fa-edit"></i> Name</td>
                  <th><i class="fa fa-edit"></i> Discription</td>
                  <th><i class="fa fa-edit"></i> QTY on Hand</td>
                  <th><i class="fa fa-edit"></i> Cost Price</td>
                  <th><i class="fa fa-edit"></i> Sales Price</td>
                  <th><i class="fa fa-edit"></i> Total Cost Price</td>
                  <th><i class="fa fa-edit"></i> Total Sale Price</td>
                  <th><i class="fa fa-edit"></i> Net-Profit</td>
                  <th><i class="fa fa-edit"></i> Date Added</td>
                  <th><i class="fa fa-edit"></i> Time Added</td>
                  <th></td>
                </tr>
              </thead>
            <cfloop query="items">
              <tbody>
             
                <cfif  #QTYavailable# < 1 >
                  <tr style="background-color:##FF0000; color:##ffffff;">
                    <td style="background-color:##FF0000;">#ID#</td>
                    <td style="background-color:##FF0000;">#ItemName#</td>
                    <td style="background-color:##FF0000;">#Description#</td>
                    <td style="background-color:##FF0000;">#QTYavailable#</td>     
                    <td style="background-color:##FF0000;"><i class="fa fa-money"></i> #CostPrice#</td>
                    <td style="background-color:##FF0000;"><i class="fa fa-money"></i> #SalesPrice#</td>
                    <td style="background-color:##FF0000;"><i class="fa fa-money"></i> #TotalCostPrice#</td>
                    <td style="background-color:##FF0000;"><i class="fa fa-money"></i> #TotalSalesPrice#</td>
                    <td style="background-color:##FF0000;"><i class="fa fa-money"></i> #Profit#</td>
                    <td style="background-color:##FF0000;"><i class="fa fa-calendar"></i> #DateAdded#</td>
                    <td style="background-color:##FF0000;"><i class="fa fa-history"></i> #TimeAdded#</td>
                    </td>
                  </tr>
                    <cfelse>
                  <tr>
                    <td>#ID#</td>
                    <td>#ItemName#</td>
                    <td>#Description#</td>
                    <td >#QTYavailable#</td>     
                    <td><i class="fa fa-money"></i> #CostPrice#</td>
                    <td><i class="fa fa-money"></i> #SalesPrice#</td>
                    <td><i class="fa fa-money"></i> #TotalCostPrice#</td>
                    <td><i class="fa fa-money"></i> #TotalSalesPrice#</td>
                    <td><i class="fa fa-money"></i> #Profit#</td>
                    <td><i class="fa fa-calendar"></i> #DateAdded#</td>
                    <td><i class="fa fa-history"></i> #TimeAdded#</td>
                    
                  </tr>
                </cfif>
              </tbody>
            </cfloop>
            
          </table>
		</div>
</div>
</div>
<!------------------------------------------------End of useer interface-------------------------------------------->

<!------------------------------------------------------modal to Add item------------------------------------------->
  <div class="modal fade" id="additem" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <h2 class="modal-title" id="myModalLabel"><i class="fa fa-plus"></i>&ensp;Add Item</h2>
          </div>
          <div class="modal-body">
            <form class="form-horizontal" method="post" role="form">
              <div class="form-horizontal">
                <div class="col-sm-6">
                  <input class="form-control" type="text" value="" name="itemname" placeholder="Enter Item Name" required>
                </div>

                <div class="col-sm-6">
                  <input class="form-control" type="text" value="" name="Quantity" placeholder="Enter Quantity On Hand" required>
                </div>
              </div>
           		<br/><br/><br/>
               <div class="form-horizontal">
                <div class="col-sm-12">
                  <textarea class="form-control" name="description" value="" placeholder="Description" required></textarea>
                </div>
              </div>
              <br/><br/><br/><br/>
              <div class="form-horizontal">
                <div class="col-sm-6">
                  <label for="title" class=" control-label">Purchase Information</label>
                </div>

                <div class="col-sm-6">
                 <label for="title" class="control-label">Sales Information</label>
                </div>
              </div>
              <br/><br/>
              <div class="form-horizontal">
                <div class="col-sm-6">
                  <input class="form-control" type="text" value="" name="costprice" placeholder="Enter cost price here" required>
                </div>

                <div class="col-sm-6">
                  <input class="form-control" type="text" value="" name="saleprice" placeholder="Enter sales price here" required>
                </div>
              </div>
              <br/><br/>
              <hr>
              <button type="submit" value="submit" class="btn btn-primary" name="submit"><i class="fa fa-floppy-o"></i>&ensp;Add to Inventory</button>
            </form>
          </div>
        </div>
      </div>
    </div>
<!-----------------------------------------------End of modal Add item-------------------------------------------------------------->

<div class="modal fade" id="Edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <h2 class="modal-title" id="myModalLabel"><i class="fa fa-edit"></i>&ensp;Edit Item</h2>
          </div>
          <div class="modal-body">
            <form class="form-horizontal" method="post" role="form">
              <div class="form-horizontal">
                 <div class="col-sm-6">
                  <select  class="form-control" name="itemname" placeholder="Select Item" required>
                    <option value="">---Select an Item---</option>
                    <cfquery name="itemsel" datasource="inventnet">
                      SELECT * FROM `items`;
                    </cfquery>
                      <cfloop query="itemsel">
                        <option>#ItemName#</option>
                      </cfloop>
                  </select>
                </div>

                <div class="col-sm-6">
                  <input class="form-control" type="text" value="" name="Quantity" placeholder="Enter Quantity On Hand" required>
                </div>
              </div>
              <br/><br/><br/>
               <div class="form-horizontal">
                <div class="col-sm-12">
                  <textarea class="form-control" name="description" value="" placeholder="Description" required></textarea>
                </div>
              </div>
              <br/><br/><br/><br/>
              <div class="form-horizontal">
                <div class="col-sm-6">
                  <label for="title" class=" control-label">Purchase Information</label>
                </div>

                <div class="col-sm-6">
                 <label for="title" class="control-label">Sales Information</label>
                </div>
              </div>
              <br/><br/>
              <div class="form-horizontal">
                <div class="col-sm-6">
                  <input class="form-control" type="text" value="" name="costprice" placeholder="Enter cost price here" required>
                </div>

                <div class="col-sm-6">
                  <input class="form-control" type="text" value="" name="saleprice" placeholder="Enter sales price here" required>
                </div>
              </div>
              <br/><br/>
              <hr>
              <button type="submit" value="edit" class="btn btn-primary" name="edit"><i class="fa fa-floppy-o"></i>&ensp; Save</button>
            </form>
          </div>
        </div>
      </div>
    </div>

<!------------------------------------------------Modal Search---------------------------------------------------------------------->
  <div class="modal fade" id="search" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <h2 class="modal-title" id="myModalLabel"><i class="fa  fa-search"></i>&ensp;Search</h2>
          </div>
          <div class="modal-body">
            <form class="form-horizontal" method="post" role="form">
              <div class="form-horizontal">
                <div class="col-sm-6">
                  <input class="form-control" type="text" value="" name="itemname"  placeholder="Item Name" required>
                </div>

                <div class="col-sm-6">
                  <input class="form-control" type="text" value="" name="category"  placeholder="Category" required>
                </div>
              </div>
              <br/><br/><br/>
              <div class="form-horizontal">

                <!---div class="col-sm-6">
                  <input data-provide="datepicker" value="" class="form-control datepicker" name="date" placeholder="Date created-yyyy/mm/dd" data-date-format="yyyy-mm-dd" data-date-startdate="-3y" required/>
                </div--->

                <!---div class="col-sm-6">
                  <input class="form-control" type="text" value="" name="category"  placeholder="Category" required>
                </div>
              </div--->
              <br/><br/>
              <hr>
              <button type="submit" value="submit" class="btn btn-primary" name="search"><i class="fa fa-search"></i>&ensp;Search</button>
            </form>
          </div>
        </div>
      </div>
    </div>
<!---------------------------------------------------End of Modal search------------------------------------------------------------------>
</cfoutput> 
