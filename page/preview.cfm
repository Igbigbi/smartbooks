<cfoutput>
	<cfparam name="h" type="string" default="novalue">
	  <cfif isdefined("form.submit")>
	  	<cfset customer="#form.CustomerName#">
	  	<cfset address = "#form.address#">
	  	<cfset email = "#CustomerEmail#">
      <cfset count="#form.index#" - 1>

      <cfset counter=1>
        <cfset h = queryNew("Item,Quantity")/>
            <cfloop from="1" to="#listlen(ItemPurchase)#" index="item">
              <cfset h.addrow()/>
                      <cfset h.SetCell("Item", '#listgetat(ItemPurchase, item)#')/>

                  <cfloop from="#counter#" to="#listlen(quantity)#" index="qty">
                    
                    <cfset h.setCell('Quantity','#listgetat(quantity, qty)#')/>

                    <cfset #counter# =#counter# + 1>
                    <cfbreak>
                  </cfloop>
            </cfloop>
             	<cfquery name="ItemBought" datasource="inventnet">
             		select * from `items`
             	</cfquery>
             	<cflock scope="Session" type="exclusive" timeout="5"throwontimeout="true">
								<cfset session.h =	"#h#" >
				</cflock>           
        </cfif>

   <br/><br/>   
	 	<div class="container">
	 		<h4 align="center" style="color:##800000;"><b>RECEIPT</b></h4><hr/>
	 		<form method="post" action="?pg=receipt">
	 			<div class="form-horizontal">
                	<label for="title" class="col-sm-1 control-label">Customer:</label>
                     <div class="col-sm-3">
                   		<h4>#customer#</h4>
                   		<input type="hidden" name="custName" value="#customer#">
                    </div>
                    <div class="col-sm-4"></div>
                    <label for="title" class="col-sm-1 control-label">Date:</label>
                     <div class="col-xs-3">
                      <label for="title" class=" control-label">#dateformat(now(), "dd/mm/yyyy")#</label>
                    </div>

                </div><br/><br/>
             	<div class="form-horizontal">
                      <label for="title" class="col-sm-1 control-label">Address:</label>
                     <div class="col-sm-3">
                     	<h4>#address#</h4>
                     	<input type="hidden" name="custAddress" value="#address#">
                    </div> 
                    <div class="col-sm-4"></div>
                	       <label for="title" class="col-sm-1 control-label">Email:</label>
                     <div class="col-sm-3">
                      	<h4>#email#</h4>
                      	<input type="hidden" name="custEmail" value="#email#">
                    </div>
                  </div><hr/><hr/>


	 		<div>
	 			<table id="table" class="table table-striped" cellspacing="0" cellpadding="0">
	 				<thead>
	 					<tr>
	 						<th>S/No.</th>
	                        <th>Item</th>
	                        <th>Description</th>
	                        <th>Price</th>
	                        <th><i class="fa fa-edit"></i> QTY</th>
	                        <th>Amount</th>
                        </tr>
	 				</thead>
					<cfset sn = 0>
					<cfset total=0>
	 				<cfloop query="#h#">
	 					<cfloop query="#ItemBought#">		
	 						<cfif #h.Item# == #ItemBought.ItemName#>
		                     	<tr>
		                     		
		                     		<td>#sn=#sn#+1#</td>
		                     		<td>#QTYavailable# #ItemName#</td>
			 						<td>#Description#</td>	
			 						<td>#salesPrice#.00</td>
			 						<cfif #QTYavailable# < #h.quantity#  >
			 							<td style="background-color:##FF0000; color:##ffffff;">#ItemBought.QTYavailable# available in stock</td>
			 							<td style="background-color:##FF0000; color:##ffffff;">NILL</td>
			 							<cfelse>
			 								

			 						<td>#h.quantity#</td>
			 						<cfset amount = #salesPrice# * #h.quantity#>
			 						<td>#amount#.00</td>
			 						<cfset total=total+#amount#>
			 						</cfif>
			 					</tr>

			 						
	 						<cfelse>
	 							
	 						</cfif>
	 					
	 					</cfloop>
	 				</cfloop>
	 				<tr>
						<td></td>
             			<td></td>
						<td></td>	
						<td></td>
						<td><h4>TOTAL AMOUNT =<h4></td>
						<td><h4>#total#.00<h4></td>
						
			 		</tr>
	 	
	 			</table>
	 			<div class="col-md-4">
	 				<a class="btn btn-primary" href="?pg=sellInventory"><i class="fa fa-chevron-left"></i><i class="fa fa-chevron-left"></i> Return to Trade desk</a>
	 			</div>
	 			<div class="col-md-4">
	 			</div>
	 			<div class="col-md-4" style="padding-left:600px">
	 				<cfset count=0>
	 			<cfloop query="#h#">
	 					<cfloop query="#ItemBought#">		
	 						<cfif #h.Item# == #ItemBought.ItemName#>
			 						<cfif #h.quantity# <= #QTYavailable# >
	 								<cfset qty="true">
	 						<cfelse>
	 								<cfset #count# = #count# + 1>
	 						</cfif>
	 						</cfif>
	 					
	 			</cfloop>
	 			</cfloop>
	 			<cfif count == 0>
	 				<button type="submit"= class="btn btn-danger" name="checkout" value="checkout"><i class="fa fa-shopping-cart"></i> Check out!</button>
	 				<cfelse>
	 			</cfif>
	 			</div>
	 		</div>
	 	</form>
	 	</div><br/><br/><br/>
</cfoutput>