<cfoutput>
   <br/><br/>   
	 		<h4 align="center" style="color:##800000;"><b>AUDIT</b></h4><hr/>
	 		<div class="container">
	 		<ul class="nav nav-tabs">
	 			<li ><a href="?pg=solditems">Items Sold</a></li> 
	 			<li><a href="?pg=purchaseaudit">Purchase Received</a></li>  
	 		</ul>

	 <h3 align="center" style="color:##800000;padding:20px;"><b>ITEMS SOLD</b></h3>
	 <cfquery name="sld" datasource="inventnet">
	 	SELECT * FROM `solditem`
	</cfquery>
    		
        <div class="col-md-12"style="padding:40px; padding-buttom:5px;" id="resultTable">
          <table class="table table-striped" cellspacing="0" cellpadding="0">
            <thead>
                <tr>
                  <th>##S.I ID.</td>
                  <th><i class="fa fa-edit"></i> Item ID</td>
                  <th><i class="fa fa-edit"></i> Name</td>
                  <th><i class="fa fa-edit"></i> Description</td>
                  <th><i class="fa fa-edit"></i> Sale Price</td>
                  <th><i class="fa fa-edit"></i> Quantity Bought</td>
                  <th><i class="fa fa-edit"></i> Total Amount</td>
                  <th><i class="fa fa-edit"></i> Date Added</td>
                  <th><i class="fa fa-edit"></i> Time Added</td>
                  <th><i class="fa fa-edit"></i> Customers</td>
                  <th><i class="fa fa-edit"></i> Customer's Email</td>



                  <th></td>
                </tr>
              </thead>
            <cfloop query="sld">
              <tbody>
           
                  <tr>
                    <td >#SoldItemId#</td>
                    <td >#ItemId#</td>
                    <td >#ItemName#</td>
                    <td >#Description#</td>     
                    <td ><i class="fa fa-money"></i> #SalesPrice#</td>
                    <td > #QuantityBought#</td>
                    <td><i class="fa fa-money"></i> #TotalAmount#</td>
                    <td> #DateSold#</td>
                    <td> #TimeSold#</td>
                    <td> #CustomerName#</td>
                    <td> #CustomerEmail#</td>
                    </td>
                  </tr>
                
              </tbody>
            </cfloop>
            
          </table>
		</div>
</div>
</div>
</cfoutput>