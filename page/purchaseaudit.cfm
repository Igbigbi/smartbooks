<cfoutput>
   <br/><br/>   
	 		<h4 align="center" style="color:##800000;"><b>AUDIT</b></h4><hr/>
	 		<div class="container">
	 		<ul class="nav nav-tabs">
	 			<li ><a href="?pg=solditems">Items Sold</a></li> 
	 			<li><a href="?pg=purchaseaudit">Purchase Received</a></li>  
	 		</ul>

	 <h3 align="center" style="color:##800000;padding:20px;"><b>PURCHASE RECEIVED</b></h3>
	 <cfquery name="prc" datasource="inventnet">
	 	SELECT * FROM `purchaseaudit`
	</cfquery>
    		
        <div class="col-md-12"style="padding:40px; padding-buttom:5px;" id="resultTable">
          <table class="table table-striped" cellspacing="0" cellpadding="0">
            <thead>
                <tr>
                  <th>##P.A ID.</td>
                  <th><i class="fa fa-edit"></i> Item ID</td>
                  <th><i class="fa fa-edit"></i> Name</td>
                  <th><i class="fa fa-edit"></i> Description</td>
                  <th><i class="fa fa-edit"></i> Quantity</td>
                  <th><i class="fa fa-edit"></i> Cost</td>
                  <th><i class="fa fa-edit"></i> Total Cost Price</td>
                  <th><i class="fa fa-edit"></i> Date Added</td>
                  <th><i class="fa fa-edit"></i> Time Added</td>
                  <th></td>
                </tr>
              </thead>
            <cfloop query="prc">
              <tbody>
           
                  <tr>
                    <td >#PurchaseAuditId#</td>
                    <td >#ItemId#</td>
                    <td >#ItemName#</td>
                    <td >#Description#</td>     
                    <td ><i class="fa fa-money"></i> #QTY#</td>
                    <td ><i class="fa fa-money"></i> #Cost#</td>
                    <td><i class="fa fa-money"></i> #TotalCost#</td>
                    <td><i class="fa fa-money"></i> #DateReceived#</td>
                    <td><i class="fa fa-money"></i> #TimeReceived#</td>
                    </td>
                  </tr>
                
              </tbody>
            </cfloop>
            
          </table>
		</div>
</div>
</div>
</cfoutput>