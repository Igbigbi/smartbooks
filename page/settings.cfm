<cfoutput>
    <script type="text/java">
        function myFunction() {
            location.reload();
        }
    </script>

		<cfquery name="user" datasource="inventnet">
			SELECT * FROM `user`;
		</cfquery>

		<!-----add User-------------->
		<cfif isdefined("form.submit")>
			
					<cfquery name="additem" datasource="inventnet">
						INSERT INTO `user` (`FirstName`, 
                                `LastName`, 
                                `Sex`, 
                                `DOB`,
                                `Address`,
                                `Country`,
                                `Email`,
                                `Password`
                              )
									VALUES (
										<cfqueryparam cfsqltype="CF_SQL_varchar" value='#form.Fname#'>, 
										<cfqueryparam cfsqltype="CF_SQL_varchar" value='#form.Lname#'>,
										<cfqueryparam cfsqltype="CF_SQL_varchar" value='#form.sex#'>,
										<cfqueryparam cfsqltype="CF_SQL_varchar" value='#form.Dateofbirth#'>,
                    <cfqueryparam cfsqltype="CF_SQL_varchar" value='#form.address#'>,
										<cfqueryparam cfsqltype="CF_SQL_varchar" value='#form.Country#'>,
                    <cfqueryparam cfsqltype="CF_SQL_varchar" value='#form.Email#'>,
                    <cfqueryparam cfsqltype="CF_SQL_varchar" value='#form.Password#'>
										)
					</cfquery><br/>
						<div style="margin-left:500px; width:450px; border-radius:10px; padding-left:100px; background-color:##4DA64D;color:##003300;"><b>You have successfully added a product</b></div>
					<cfquery name="items" datasource="inventnet">
							SELECT * FROM `user`;
					</cfquery>
					<script type="text/java">
					$('##myModal').on('hide.bs.modal', function () {
   						$('##myModal').removeData();
						})
					</script>
			</cfif>
	
<br/><br/>
<!---------------------------------user interface-------------------------------------------->
<div style="margin-top:0px;background-color:##fff;padding:20px; position:fixed;z-index:1000; width:100%; box-shadow: 2px 3px 3px   ##ccc;">
    <h3 align="center" style="color:##800000;"><b><i class="fa fa-"></i> SETTINGS</b></h3>

    		<div class="col-md-3">
    			<form>
    				<div class="form-group">
    					<input class="form-control" type="text" name="search" value="" placeholder="Search..."></input>
    				</div>
    			</form>
    		</div>
    		<div class="col-md-4"></div>
    		<div class="col-md-5" style="padding-left:90px;">

          
    			
             <a href="?pg=settings"><button type="button" class="btn btn-default"><i class="fa fa-refresh"></i> Refresh</button></a>
            
            <button type="button" data-toggle="modal" data-target="##additem" data-refresh="true" class="btn btn-danger"><i class="fa fa-plus"></i> Add User</button> 
    				<button type="button" data-toggle="modal" data-target="##search" data-refresh="true" class="btn btn-primary"><i class="fa fa-search"></i> Search User</button> 
    			
          </div>
          </div><br/><br/>
    		<br/>    
      </div>
  <div style="margin-top:150px;">
        <div class="col-md-12"style="padding:40px; padding-buttom:5px;" id="resultTable">
          <table class="table table-striped" cellspacing="0" cellpadding="0">
            <thead>
                <tr>
                  <th><i class="fa fa-user"></i> User ID.</td>
                  <th><i class="fa fa-user"></i> FirstName</td>
                  <th><i class="fa fa-user"></i> LastName</td>
                  <th><i class="fa fa-male"></i>/<i class="fa fa-male"></i> Sex</td>
                  <th><i class="fa fa-calendar"></i> Date of birth</td>
                  <th><i class="fa fa-user"></i> Address</td>
                  <th><i class="fa fa-user"></i> Country</td>
                  <th><i class="fa fa-user"></i> Email</td>
                  <th><i class="fa fa-user"></i> Password</td>
                </tr>
              </thead>
            <cfloop query="user">
              <tbody>
              <tr>
                <td>#ID#</td>
                <td>#FirstName#</td>
                <td>#LastName#</td>
                <cfif #Sex# == "female"><td><i class="fa fa-female"></i>  #Sex#</td> 
                	<cfelse>
                		<td><i class="fa fa-male"></i>  #Sex#</td>
                </cfif>
                <td>#DOB#</td>
                <td>#Address#</td>
                <td>#Country#</td>
                <td>#Email#</td>
                <td>#Password#</td>
              </tr>
            </cfloop>
            </tbody>
          </table>
		</div>
</div>
</div>
</div>
<!------------------------------------------------End of useer interface-------------------------------------------->

<!------------------------------------------------------modal to Add item------------------------------------------->
  <div class="modal fade" id="additem" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <h2 class="modal-title" id="myModalLabel"><i class="fa fa-user"></i>&ensp;Add User</h2>
          </div>
          <div class="modal-body">
            <form class="form-horizontal" method="post" role="form">
              <div class="form-horizontal">
                <div class="col-sm-6">
                  <input class="form-control" type="text" value="" name="Fname" placeholder="Enter User's FirstName" required>
                </div>

                <div class="col-sm-6">
                  <input class="form-control" type="text" value="" name="Lname" placeholder="Enter User's LastName" required>
                </div>
              </div>
           		<br/><br/><br/>
               <div class="form-horizontal">
                <div class="col-sm-12">
                  <textarea class="form-control" name="address" value="" placeholder="Enter Users Address" required></textarea>
                </div>
              </div>
              <br/><br/><br/><br/>
              <div class="form-horizontal">
                <div class="col-sm-6">
                  <input class="form-control" type="text" value="" name="Dateofbirth" placeholder="Enter Date Of Birth" required>
                </div>

                <div class="col-sm-3">
                <select class="form-control" name="sex" value="">
                	<option value="Male">Male</option>
                	<option value="Female">Female</option>
                </select>
                </div>
                <div class="col-sm-3">
                  <input class="form-control" type="text" value="" name="Password" placeholder="Password" required>
                </div>
              </div>
              <br/><br/><br/>
              <div class="form-horizontal">
                <div class="col-sm-6">
                  <input class="form-control" type="text" value="" name="country" placeholder="Country" required>
                </div>

                <div class="col-sm-6">
                  <input class="form-control" type="Email" value="" name="Email" placeholder="Email" required>
                </div>
              </div>
              <br/><br/>
              <hr>
              <button type="submit" value="submit" class="btn btn-primary" name="submit"><i class="fa fa-floppy-o"></i>&ensp;Add User</button>
            </form>
          </div>
        </div>
      </div>
    </div>
<!-----------------------------------------------End of modal Add item-------------------------------------------------------------->
<!------------------------------------------------Modal Search---------------------------------------------------------------------->
  <div class="modal fade" id="search" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" style="background-color:##800000;"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <h2 class="modal-title" id="myModalLabel"><i class="fa  fa-search"></i>&ensp;Search</h2>
          </div>
          <div class="modal-body">
            <form class="form-horizontal" method="post" role="form">
              <div class="form-horizontal">
                <div class="col-sm-6">
                  <input class="form-control" type="text" value="" name="itemname"  placeholder="Item Name" required>
                </div>

                <div class="col-sm-6">
                  <input class="form-control" type="text" value="" name="category"  placeholder="Category" required>
                </div>
              </div>
              <br/><br/><br/>
              <div class="form-horizontal">

                <!---div class="col-sm-6">
                  <input data-provide="datepicker" value="" class="form-control datepicker" name="date" placeholder="Date created-yyyy/mm/dd" data-date-format="yyyy-mm-dd" data-date-startdate="-3y" required/>
                </div--->

                <!---div class="col-sm-6">
                  <input class="form-control" type="text" value="" name="category"  placeholder="Category" required>
                </div>
              </div--->
              <br/><br/>
              <hr>
              <button type="submit" value="submit" class="btn btn-primary" name="search"><i class="fa fa-search"></i>&ensp;Search</button>
            </form>
          </div>
        </div>
      </div>
    </div>
<!---------------------------------------------------End of Modal search------------------------------------------------------------------>
</cfoutput> 

