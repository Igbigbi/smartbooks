<cfoutput>
<!---check out--------------------------------------------->
	<cfif isdefined("form.checkout")>
		
		 <cfquery name="items" datasource="inventnet">
        	SELECT * FROM `customer` WHERE `Name`= "#form.custName#";
     	 </cfquery>
     	 <cfif items.recordcount greater than  0>

     	 <cfelse>
			<cfquery name="checkout" datasource="inventnet">
				INSERT INTO `customer` (`Name`, 
	                                	`Address`, 
	                               		`Email`)
	                  VALUES (
	                    <cfqueryparam cfsqltype="CF_SQL_varchar" value='#form.custName#'>, 
	                    <cfqueryparam cfsqltype="CF_SQL_varchar" value='#form.custAddress#'>,
	                    <cfqueryparam cfsqltype="CF_SQL_varchar" value='#form.custEmail#'>
	                    )
			</cfquery>
		</cfif>
		<cfquery name="Itembuy" datasource="inventnet">
             		select * from `items`
        </cfquery>
			<cfloop query="#session.h#">
	 					<cfloop query="#Itembuy#">		
	 						<cfif #session.h.Item# == #Itembuy.ItemName#>
	 							<cfset itemRemain = #Itembuy.QTYavailable# - #session.h.Quantity#>
	 							<cfset costNew= #itemRemain# * #Itembuy.CostPrice#>
          						<cfset salesNew= #itemRemain# * #Itembuy.SalesPrice#>
          						<cfset profitNew= #salesNew# - #costNew#>
          						<cfset totalamount = #session.h.Quantity# * #Itembuy.SalesPrice#>
		                     	
		                     	<cfquery name="checkoutItems" datasource="inventnet">
		                     		UPDATE `Items`
            						SET QTYavailable = '#itemRemain#',
                						TotalCostPrice = '#costNew#',
                						TotalSalesPrice = '#salesNew#',
                						Profit='#profitNew#'
             						WHERE `ItemName`="#session.h.Item#";
								</cfquery>
								<cfquery name="Items" datasource="inventnet">
						            INSERT INTO `solditem` (`ItemId`, 
						                                	`ItemName`, 
						                               		`Description`,
						                               		`SalesPrice`,
						                               		`QuantityBought`,
						                               		`TotalAmount`,
						                               		`DateSold`,
						                               		`TimeSold`,
						                               		`CustomerName`,
						                               		`CustomerEmail`)
						                  VALUES (
						                    <cfqueryparam cfsqltype="CF_SQL_INTEGER" value='#Itembuy.ID#'>, 
						                    <cfqueryparam cfsqltype="CF_SQL_varchar" value='#Itembuy.ItemName#'>,
						                    <cfqueryparam cfsqltype="CF_SQL_varchar" value='#Itembuy.Description#'>,
						                    <cfqueryparam cfsqltype="CF_SQL_INTEGER" value='#Itembuy.SalesPrice#'>,
						                    <cfqueryparam cfsqltype="CF_SQL_INTEGER" value='#session.h.Quantity#'>,
						                    <cfqueryparam cfsqltype="CF_SQL_INTEGER" value='#totalamount#'>,
						                    <cfqueryparam cfsqltype="CF_SQL_varchar" value='#dateformat(now(), "yyyy-mm-dd")#'>,
                							<cfqueryparam cfsqltype="CF_SQL_varchar" value= '#TimeFormat(now(), "HH:MM:SS")#'>,
						                    <cfqueryparam cfsqltype="CF_SQL_varchar" value='#form.custName#'>,
                							<cfqueryparam cfsqltype="CF_SQL_varchar" value='#form.custEmail#'>
                							)
						        </cfquery>	
	 						<cfelse>
	 							
	 						</cfif>
	 					
	 					</cfloop>
	 				</cfloop>


   <br/><br/>  
	 	<div class="container">
	 		<h4 align="center" style="color:##800000;"><b>RECEIPT</b></h4><hr/>
	 		<div style="margin-left:1070px;">
	 		<button type="submit"= class="btn btn-danger" name="checkout" onclick="javascript:window.print()" value="checkout"><i class="fa fa-print"></i> Print</button>
	 		</div>
	 		<form method="post">
	 			<div class="form-horizontal">
                	<label for="title" class="col-sm-1 control-label">Customer:</label>
                     <div class="col-sm-3">
                   		<h4>#form.custName#</h4>
                   		
                    </div>
                    <div class="col-sm-4"></div>
                    <label for="title" class="col-sm-1 control-label">Date:</label>
                     <div class="col-xs-3">
                      <label for="title" class=" control-label">#dateformat(now(), "dd/mm/yyyy")#</label>
                    </div>

                </div><br/><br/>
             	<div class="form-horizontal">
                      <label for="title" class="col-sm-1 control-label">Address:</label>
                     <div class="col-sm-3">
                     	<h4>#form.custAddress#</h4>
                     
                    </div> 
                    <div class="col-sm-4"></div>
                	       <label for="title" class="col-sm-1 control-label">Email:</label>
                     <div class="col-sm-3">
                      	<h4>#form.custEmail#</h4>
                      	
                    </div>
                  </div><hr/><hr/>


	 		<div>
	 			<table id="table" class="table table-striped" cellspacing="0" cellpadding="0">
	 				<thead>
	 					<tr>
	 						<th>S/No.</th>
	                        <th>Item</th>
	                        <th>Description</th>
	                        <th>Price</th>
	                        <th><i class="fa fa-edit"></i> QTY</th>
	                        <th>Amount</th>
                        </tr>
	 				</thead>
					<cfset sn = 0>
					<cfset total=0>
	 				<cfloop query="#session.h#">
	 					<cfloop query="#Itembuy#">		
	 						<cfif #session.h.Item# == #Itembuy.ItemName#>
		                     	<tr>
		                     		
		                     		<td>#sn=#sn#+1#</td>
		                     		<td>#ItemName#</td>
			 						<td>#Description#</td>	
			 						<td>#salesPrice#.00</td>
			 						<td>#session.h.quantity#</td>
			 						<cfset amount = #salesPrice# * #session.h.quantity#>
			 						<td>#amount#.00</td>
			 						<cfset total=total+#amount#>
			 					</tr>

			 						
	 						<cfelse>
	 							
	 						</cfif>
	 					
	 					</cfloop>
	 				</cfloop>
	 				<tr>
						<td></td>
             			<td></td>
						<td></td>	
						<td></td>
						<td><h4>TOTAL AMOUNT =<h4></td>
						<td><h4>#total#.00<h4></td>
						
			 		</tr>
	 	
	 			</table>
	 		</div>
	 	</form>
	 	</div><br/><br/><br/>

	 	
	</cfif>
<cfset #Itembuy.ID# ="">
<cfset #Itembuy.ItemName# ="">
<cfset #Itembuy.Description# ="">
<cfset #Itembuy.SalesPrice# ="">
<cfset #session.h.Quantity# ="">
<cfset #totalamount# ="">
<cfset#form.custEmail# ="">
<cfset #form.custEmail# ="">
<cfset #Itembuy.ID# ="">

<!---/check out--------------------------------------------->
</cfoutput>